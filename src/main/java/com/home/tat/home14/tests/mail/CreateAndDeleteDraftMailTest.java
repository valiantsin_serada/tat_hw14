package com.home.tat.home14.tests.mail;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.home.tat.home14.lib.common.builders.AccountBuilder;
import com.home.tat.home14.lib.common.constants.ICommonConstants;
import com.home.tat.home14.lib.common.models.Account;
import com.home.tat.home14.lib.feature.mail.builders.LetterBuilder;
import com.home.tat.home14.lib.feature.mail.models.Letter;
import com.home.tat.home14.lib.feature.mail.service.LoginService;
import com.home.tat.home14.lib.feature.mail.service.MailService;

public class CreateAndDeleteDraftMailTest {
	private Account account = AccountBuilder.getDefaultAccount();
	private LoginService loginService = new LoginService();
	private MailService mailService = new MailService();
	private Letter letter;

	@BeforeClass
	public void prepareData() {
		letter = LetterBuilder.getRandomLetter();
	}

	@Test
	public void createAndDeleteDraft() {
		loginService.loginToMailbox(account);
		mailService.createAndDeleteDraft(letter);
		Assert.assertEquals(mailService.getMessageAboutDeleteLetter(), ICommonConstants.LETTER_DELETED_MESSAGE, "The message about deleting draft is different, then expected");
	}
}
