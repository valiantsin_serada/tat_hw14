package com.home.tat.home14.tests.mail;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.home.tat.home14.lib.common.builders.AccountBuilder;
import com.home.tat.home14.lib.common.constants.ICommonConstants;
import com.home.tat.home14.lib.common.models.Account;
import com.home.tat.home14.lib.feature.mail.builders.LetterBuilder;
import com.home.tat.home14.lib.feature.mail.models.Letter;
import com.home.tat.home14.lib.feature.mail.service.LoginService;
import com.home.tat.home14.lib.feature.mail.service.MailService;

public class SendUnfilledAddressFieldTest {
	private Account account = AccountBuilder.getDefaultAccount();
	private LoginService loginService = new LoginService();
	private MailService mailService = new MailService();
	private Letter letter;

	@BeforeClass
	public void prepareData() {
		letter = LetterBuilder.getLetterWithoutAddress();
	}

	@Test
	public void sendMail() {
		loginService.loginToMailbox(account);
		mailService.sendUnaddressedMail(letter);
		Assert.assertEquals(
				mailService.getErrorMessageInCaseOfEmptyAddressField(),
				ICommonConstants.EMPTY_ADDRESS_ERROR_MESSAGE,
				"Error message is different, then expected");
	}
}
