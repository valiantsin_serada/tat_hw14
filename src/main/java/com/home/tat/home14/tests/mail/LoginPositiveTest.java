package com.home.tat.home14.tests.mail;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.home.tat.home14.lib.common.builders.AccountBuilder;
import com.home.tat.home14.lib.common.models.Account;
import com.home.tat.home14.lib.feature.mail.service.LoginService;

public class LoginPositiveTest {
	private Account account = AccountBuilder.getDefaultAccount();

	@Test
	public void loginPositiveTest() {
		LoginService loginService = new LoginService();
		loginService.loginToMailbox(account);
		Assert.assertTrue(loginService.checkSuccessLogin(account),"Login failed whith credentials: " + account.getLogin() + " password: " + account.getPassword());
	}
}
