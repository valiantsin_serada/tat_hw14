package com.home.tat.home14.tests.disk;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.home.tat.home14.lib.common.builders.AccountBuilder;
import com.home.tat.home14.lib.common.models.Account;
import com.home.tat.home14.lib.feature.disk.builders.FileResourceBuilder;
import com.home.tat.home14.lib.feature.disk.models.FileResource;
import com.home.tat.home14.lib.feature.disk.service.DiskService;
import com.home.tat.home14.lib.util.FileTools;

public class FileToTrashTest {
	private FileResource testFileResource = FileResourceBuilder
			.getFileResource();
	private Account account = AccountBuilder.getDefaultAccount();
	private DiskService diskService = new DiskService();

	@BeforeClass
	public void prepareData() throws IOException {
		FileTools.createTestFile(testFileResource);
	}

	@Test
	public void uploadTest() {
		diskService.loginToDisk(account);
		diskService.openFolder();
		diskService.uploadFile(testFileResource.getFileName());
		Assert.assertTrue(
				diskService.isFileInFolder(testFileResource.getFileName()),
				"The file is not present on the disk page");
	}

	@Test(dependsOnMethods = { "uploadTest" })
	public void sendToTrashTest() {
		diskService.dropeFilesToTrash(FileTools
				.wrapFileResourceByList(testFileResource));
		Assert.assertTrue(
				diskService.isFileInTrash(testFileResource.getFileName()),
				"The file is not present on the trash page");
	}

	@Test(dependsOnMethods = { "sendToTrashTest" })
	public void deleteFileTest() {
		diskService.deleteFilePermanently(testFileResource);
		Assert.assertTrue(diskService.isFileDeletedPermanently(testFileResource
				.getFileName()), "The file is still present on the trash page");
	}

	@AfterClass
	public void deleteTestFile() throws IOException {
		Files.deleteIfExists(Paths.get(FileTools
				.getUploadedFilePath(testFileResource)));
	}
}
