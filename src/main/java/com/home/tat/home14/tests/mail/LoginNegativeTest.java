package com.home.tat.home14.tests.mail;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.home.tat.home14.lib.common.builders.AccountBuilder;
import com.home.tat.home14.lib.common.constants.ICommonConstants;
import com.home.tat.home14.lib.common.models.Account;
import com.home.tat.home14.lib.feature.mail.service.LoginService;

public class LoginNegativeTest {
	private Account account = AccountBuilder.getAccountWithWrongLogin();

	@Test
	public void loginNegativeTest() {
		LoginService loginService = new LoginService();
		loginService.loginToMailbox(account);
		Assert.assertTrue(loginService
				.retrieveErrorOnFailedLogin(ICommonConstants.PASSPORT_PAGE_ERROR_MESSAGE),"Error messagge is different then expected");
	}
}
