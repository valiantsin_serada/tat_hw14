package com.home.tat.home14.tests.disk;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.home.tat.home14.lib.common.builders.AccountBuilder;
import com.home.tat.home14.lib.common.models.Account;
import com.home.tat.home14.lib.feature.disk.builders.FileResourceBuilder;
import com.home.tat.home14.lib.feature.disk.models.FileResource;
import com.home.tat.home14.lib.feature.disk.service.DiskService;
import com.home.tat.home14.lib.util.FileTools;

public class SeveralFilesToTrashTest {
	private List<FileResource> listFileResources=FileResourceBuilder.getSeveralFileResources();
	private Account account = AccountBuilder.getDefaultAccount();
	private DiskService diskService = new DiskService();

	@BeforeClass
	public void severalFilesUpload() throws IOException {
		FileTools.createTestFiles(listFileResources);
		diskService.loginToDisk(account);
		diskService.openFolder();
		for(FileResource fileResource:listFileResources){
		diskService.uploadFile(fileResource.getFileName());
		}
	}

	@Test
	public void FilesToTrashTest() {
		diskService.dropeFilesToTrash(listFileResources);
		Assert.assertTrue(diskService.isFilesInTrash(listFileResources), "The files are not present on the trash page");
	}

	@AfterClass
	public void severalFilesDelete() throws IOException {
		diskService.deleteFiles(listFileResources);
		for (FileResource resource : listFileResources) {
			Files.deleteIfExists(Paths.get(FileTools.getUploadedFilePath(resource)));
		}
	}
}
