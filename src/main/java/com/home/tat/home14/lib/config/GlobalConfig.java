package com.home.tat.home14.lib.config;

import java.util.List;

import lombok.Data;

import org.kohsuke.args4j.Option;
import org.testng.xml.XmlSuite.ParallelMode;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;

import com.home.tat.home14.lib.ui.BrowserType;

@Data
public class GlobalConfig {
	public final static int DEFAULT_THREAD_COUNT = 0;
	public final static String DEFAULT_REMOTE_PORT = "4444";
	public static final String REMOTE_SERVER_ADDRESS_FORMAT = "http://%s:%s/wd/hub";
	private static GlobalConfig instance;

	@Option(name = "-bt", usage = "browser type: firefox or chrome")
	private BrowserType browserType = BrowserType.CHROME;

	@Option(name = "-suites", usage = "list of pathes to suites", handler = StringArrayOptionHandler.class, required = true)
	private List<String> suites = null;

	@Option(name = "-pm", usage = "parallel mode: false or tests")
	private ParallelMode parallelMode = ParallelMode.FALSE;

	@Option(name = "-tc", usage = "amount of threads for parallel execution, equal to 0 by default")
	private int threadCount = DEFAULT_THREAD_COUNT;

	@Option(name = "-host", usage = "selenium host")
	private String seleniumHost = "localhost";

	@Option(name = "-port", usage = "selenium port")
	private String seleniumPort = DEFAULT_REMOTE_PORT;

	@Option(name = "-result_dir", usage = "Directory to put results")
	private String resultDir = "results";

	public static GlobalConfig getInstance() {
		if (instance == null) {
			instance = new GlobalConfig();
		}
		return instance;
	}

	public String getRemoteServerUrl() {
		return String.format(REMOTE_SERVER_ADDRESS_FORMAT, seleniumHost,
				seleniumPort);
	}
}
