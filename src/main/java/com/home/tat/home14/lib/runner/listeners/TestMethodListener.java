package com.home.tat.home14.lib.runner.listeners;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import com.home.tat.home14.lib.common.constants.ILogingMessages;
import com.home.tat.home14.lib.util.Logger;

public class TestMethodListener implements IInvokedMethodListener {

	public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
		if (method.isTestMethod()) {
			Logger.info(ILogingMessages.START_TEST_METHOD.concat(method
					.getTestMethod().getMethodName()));
		} else {
			Logger.info(ILogingMessages.START_CONFIGURATION_METHOD
					.concat(method.getTestMethod().getMethodName()));
		}
	}

	public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
		if (method.isTestMethod()) {
			Logger.info(ILogingMessages.FINISH_TEST_METHOD.concat(method
					.getTestMethod().getMethodName()));
		} else {
			Logger.info(ILogingMessages.FINISH_CONFIGURATION_METHOD
					.concat(method.getTestMethod().getMethodName()));
		}
	}
}
