package com.home.tat.home14.lib.feature.mail.screen;

import org.openqa.selenium.By;

import com.home.tat.home14.lib.common.constants.ILogingMessages;
import com.home.tat.home14.lib.feature.mail.models.Letter;
import com.home.tat.home14.lib.ui.Browser;
import com.home.tat.home14.lib.util.Logger;

public class ComposePage {
	public static final By COMPOSE_BUTTON_LOCATOR = By
			.xpath("//a[@href='#compose']");
	public static final By TO_INPUT_LOCATOR = By
			.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
	public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
	public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
	public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
	public static final By BUTTON_SAVE_LOCATOR = By
			.xpath("//button[@data-action='dialog.save']");
	public static final By NEW_MESSAGE_LOCATOR = By
			.xpath("//a[@class='b-statusline__link']");
	public static final By UNFILLED_ADDRESS_FIELD_ALERT_LOCATOR = By
			.xpath("//span[@class='b-notification b-notification_error b-notification_error_required']");
	public static final By POPUP_TABLE_LOCATOR = By
			.xpath("//table[@class='b-popup__box']");
	public static final By DRAFTBOX_LINK_LOCATOR = By
			.xpath("//a[@href='#draft']");
	public static final By DELETE_BUTTON_LOCATOR = By
			.xpath("//a[@data-action='compose.delete']");
	public static final int ID_LENGTH = 19;
	public static final String HREF_ATRIBUTE = "href";
	public Browser browser;
	
	public ComposePage(){
		browser=Browser.current();
	}
	 
	public void open() {
		browser = Browser.current();
		browser.click(COMPOSE_BUTTON_LOCATOR);
	}

	public InboxPage sendMail(Letter letter) {
		browser = Browser.current();
		browser.writeText(TO_INPUT_LOCATOR, letter.getAddress());
		browser.writeText(SUBJECT_INPUT_LOCATOR, letter.getSubject());
		browser.writeText(MAIL_TEXT_LOCATOR, letter.getBody());
		browser.click(SEND_MAIL_BUTTON_LOCATOR);
		browser.waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
		setIdLetter(letter);
		return new InboxPage();
	}

	public ComposePage sendUnaddressedMail(Letter letter) {
		browser = Browser.current();
		browser.writeText(TO_INPUT_LOCATOR, letter.getAddress());
		browser.writeText(SUBJECT_INPUT_LOCATOR, letter.getSubject());
		browser.writeText(MAIL_TEXT_LOCATOR, letter.getBody());
		browser.click(SEND_MAIL_BUTTON_LOCATOR);
		return this;
	}

	public DraftboxPage createDraft(Letter letter) {
		browser = Browser.current();
		browser.writeText(TO_INPUT_LOCATOR, letter.getAddress());
		browser.writeText(SUBJECT_INPUT_LOCATOR, letter.getSubject());
		browser.writeText(MAIL_TEXT_LOCATOR, letter.getBody());
		browser.click(DRAFTBOX_LINK_LOCATOR);
		skipAlertMessage();
		return new DraftboxPage();
	}

	public DraftboxPage deleteDraft() {
		Browser.current().click(DELETE_BUTTON_LOCATOR);
		return new DraftboxPage();
	}

	public void setIdLetter(Letter letter) {
		Browser.current().waitForAppear(NEW_MESSAGE_LOCATOR);
		String hrefNewMailLink = Browser.current().getElementAttribute(
				NEW_MESSAGE_LOCATOR, HREF_ATRIBUTE);
		String idNewMessage = hrefNewMailLink.substring(hrefNewMailLink
				.length() - ID_LENGTH);
		letter.setId(idNewMessage);
	}

	public String getErrorMessageInCaseOfEmptyAddressField() {
		try {
			Browser.current().waitForAppear(
					UNFILLED_ADDRESS_FIELD_ALERT_LOCATOR);
		} catch (Exception e) {
			Logger.error(ILogingMessages.ERROR_OF_UNADDRESSED_MAIL_SENDING, e);
		}
		return Browser.current().getText(UNFILLED_ADDRESS_FIELD_ALERT_LOCATOR);
	}

	public void skipAlertMessage() {
		Browser.current().click(BUTTON_SAVE_LOCATOR);
	}
}
