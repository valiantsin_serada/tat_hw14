package com.home.tat.home14.lib.report;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.MDC;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.IResultListener2;
import org.testng.xml.XmlSuite;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import static com.home.tat.home14.lib.config.GlobalConfig.*;
import static com.home.tat.home14.lib.report.IMDcContext.REPORT_METHOD;
import static com.home.tat.home14.lib.report.IMDcContext.REPORT_TEST;

public class ExtentReportListener implements IResultListener2, IReporter {

    volatile static ExtentReports reports = new ExtentReports(getInstance().getResultDir() + File.separator + "report.html", true);

    public void beforeConfiguration(ITestResult result) {
        onTestStart(result);
    }

    public void onConfigurationSuccess(ITestResult result) {
        onTestSuccess(result);
    }

    public void onConfigurationFailure(ITestResult result) {
        onTestFailure(result);
    }

    public void onConfigurationSkip(ITestResult result) {
        onTestSkipped(result);
    }

    public void onTestStart(ITestResult result) {
        ExtentTest test = (ExtentTest) MDC.get(REPORT_TEST);
        ExtentTest method = reports.startTest(result.getName());
        method.setStartedTime(new Date(System.nanoTime()));
        test.appendChild(method);
        MDC.put(REPORT_METHOD, method);
    }

    public void onTestSuccess(ITestResult result) {
        ExtentTest method = (ExtentTest) MDC.get(REPORT_METHOD);
        method.setEndedTime(new Date(System.nanoTime()));
        method.log(LogStatus.PASS, "green");
        reports.endTest(method);
        MDC.remove(REPORT_METHOD);
    }

    public void onTestFailure(ITestResult result) {
        ExtentTest method = (ExtentTest) MDC.get(REPORT_METHOD);
        method.setEndedTime(Calendar.getInstance().getTime());
        method.log(LogStatus.FAIL, "fail");
        method.log(LogStatus.FAIL, result.getThrowable());
        reports.endTest(method);
        MDC.remove(REPORT_METHOD);
    }

    public void onTestSkipped(ITestResult result) {
        ExtentTest method = (ExtentTest) MDC.get(REPORT_METHOD);
        method.setEndedTime(Calendar.getInstance().getTime());
        method.log(LogStatus.SKIP, "skip");
        reports.endTest(method);
        MDC.remove(REPORT_METHOD);
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
      
    }

    public void onStart(ITestContext context) {
        ExtentTest test = reports.startTest(context.getName());
        test.setStartedTime(Calendar.getInstance().getTime());
        MDC.put(REPORT_TEST, test);
    }

    public void onFinish(ITestContext context) {
        ExtentTest test = (ExtentTest) MDC.get(REPORT_TEST);
        test.setEndedTime(Calendar.getInstance().getTime());
        reports.endTest(test);
        MDC.remove(REPORT_TEST);
    }

    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
        reports.flush();
        reports.close();
    }
}