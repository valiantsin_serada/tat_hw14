package com.home.tat.home14.lib.runner.listeners;

import org.testng.ISuite;
import org.testng.ISuiteListener;

import com.home.tat.home14.lib.config.GlobalConfig;

public class ParallelSuitesListener implements ISuiteListener {

	public void onStart(ISuite suite) {
		suite.getXmlSuite().setParallel(
				GlobalConfig.getInstance().getParallelMode());
		suite.getXmlSuite().setThreadCount(
				GlobalConfig.getInstance().getThreadCount());
	}

	public void onFinish(ISuite suite) {

	}
}
