package com.home.tat.home14.lib.feature.mail.models;

import lombok.Data;

@Data
public class Letter {
	private String id;
	private String address;
	private String subject;
	private String body;

	public Letter(String address, String subject, String body) {
		this.address = address;
		this.subject = subject;
		this.body = body;
	}
}
