package com.home.tat.home14.lib.feature.mail.builders;

import org.apache.commons.lang3.StringUtils;

import com.home.tat.home14.lib.common.constants.ICommonConstants;
import com.home.tat.home14.lib.feature.mail.models.Letter;
import com.home.tat.home14.lib.util.Randomizer;

public class LetterBuilder {
	
	public static Letter getRandomLetter() {
		String mailTo = ICommonConstants.DEFAULT_MAIL_TO_SEND;
		String mailSubject = Randomizer.alphabetic();
		String mailContent = Randomizer.alphabetic();
		return new Letter(mailTo, mailSubject, mailContent);
	}

	public static Letter getLetterWithoutAddress() {
		String mailTo = StringUtils.EMPTY;
		String mailSubject = Randomizer.alphabetic();
		String mailContent = Randomizer.alphabetic();
		return new Letter(mailTo, mailSubject, mailContent);
	}

	public static Letter getLetterWitEmptySubjectAndBody() {
		String mailTo = ICommonConstants.DEFAULT_MAIL_TO_SEND;
		String mailSubject = StringUtils.EMPTY;
		String mailContent = StringUtils.EMPTY;
		return new Letter(mailTo, mailSubject, mailContent);
	}
}
