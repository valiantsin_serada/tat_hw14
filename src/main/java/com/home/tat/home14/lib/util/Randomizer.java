package com.home.tat.home14.lib.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

public class Randomizer {
	public static final int MIN_FILES_NUMBER=2;
	public static final int DEFAULT_LENGTH = 7;
	public static final int MAX_FILES_NUMBER=5;

	public static String alphabetic() {
		return RandomStringUtils.randomAlphabetic(DEFAULT_LENGTH);
	}

	public static int number(int startInc, int endExc) {
		return RandomUtils.nextInt(startInc, endExc);
	}
	
	public static int getRundomFilesNumber() {
		return RandomUtils.nextInt(MIN_FILES_NUMBER, MAX_FILES_NUMBER);
	}	
}
