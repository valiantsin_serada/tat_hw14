package com.home.tat.home14.lib.runner;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.testng.ITestNGListener;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.Parser;
import org.testng.xml.XmlSuite;
import org.xml.sax.SAXException;

import com.home.tat.home14.lib.common.constants.ILogingMessages;
import com.home.tat.home14.lib.config.GlobalConfig;
import com.home.tat.home14.lib.report.ExtentReportListener;
import com.home.tat.home14.lib.report.TestNGLoggerListener;
import com.home.tat.home14.lib.runner.listeners.ParallelSuitesListener;
import com.home.tat.home14.lib.runner.listeners.TestEventsListener;
import com.home.tat.home14.lib.runner.listeners.TestMethodListener;
import com.home.tat.home14.lib.util.Logger;

public class TestRunner {
	public static final String NAME_XML_SUITE = "TmpSuite";

	public TestRunner(String[] args) {
		parseCli(args);
	}

	public static void main(String[] args) throws CmdLineException, ParserConfigurationException, SAXException, IOException {
		new TestRunner(args).runTests();
	}

	private void parseCli(String[] args) {
		final int ERROR_EXIT_PARAMETER = 1;
		Logger.info(ILogingMessages.PARSING_CLI_PARAMETERS);
		CmdLineParser parser = new CmdLineParser(GlobalConfig.getInstance());
		try {
			parser.parseArgument(args);
		} catch (CmdLineException e) {
			Logger.error(ILogingMessages.ERROR_OF_PARSING_CLI_PARAMETERS
					.concat(e.getMessage()), e);
			parser.printUsage(System.out);
			System.exit(ERROR_EXIT_PARAMETER);
		}
	}

	private void runTests() throws ParserConfigurationException, SAXException, IOException {
		TestNG testNG = new TestNG();
		addListeners(testNG);
		configureSuites(testNG);
		XmlSuite suite = new XmlSuite();
		suite.setName(NAME_XML_SUITE);

		List<String> files = new ArrayList<String>();
		files.addAll(GlobalConfig.getInstance().getSuites());
		suite.setSuiteFiles(files);

		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		suites.add(suite);
		testNG.setXmlSuites(suites);

		Logger.info(ILogingMessages.START_OF_TESTS);
		testNG.run();
	}

	private void configureSuites(TestNG testNG)
			throws ParserConfigurationException, SAXException, IOException {
		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		for (String suitePath : GlobalConfig.getInstance().getSuites()) {
			InputStream suiteInClassPath = getSuiteInputStream(suitePath);
			if (suiteInClassPath != null) {
				suites.addAll(new Parser(suiteInClassPath).parse());
			} else {
				suites.addAll(new Parser(suitePath).parse());
			}
		}
		for (XmlSuite xmlSuite : suites) {
			testNG.setCommandLineSuite(xmlSuite);
		}
	}

	private void addListeners(TestNG testNG) {
		testNG.setUseDefaultListeners(false);
		List<ITestNGListener> listeners = new ArrayList<ITestNGListener>() {
			{
				add(new ExtentReportListener());
				add(new ParallelSuitesListener());
				add(new TestNGLoggerListener());
				add(new TestListenerAdapter());
				add(new TestEventsListener());
				add(new TestMethodListener());
			}
		};
		for (ITestNGListener listener : listeners) {
			testNG.addListener(listener);
		}
	}

	private InputStream getSuiteInputStream(String suite) {
		InputStream resourceAsStream = this.getClass().getClassLoader()
				.getResourceAsStream(suite);
		return resourceAsStream;
	}
}
