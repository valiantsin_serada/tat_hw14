package com.home.tat.home14.lib.feature.mail.screen;

import org.openqa.selenium.By;

import com.home.tat.home14.lib.feature.mail.models.Letter;
import com.home.tat.home14.lib.ui.Browser;

public class TrashboxPage implements IPageInterface {
	public static final By TRASHBOX_LINK_LOCATOR = By
			.xpath("//a[@href='#trash']");
	public static final By DELETED_LETTER_MESSAGE_LOCATOR = By
			.xpath("//div[@class='b-statusline']//ancestor::span[@class='b-statusline__content']");
	public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
	public static final String HREF_ATRIBUTE = "href";
	public Browser browser;

	public TrashboxPage(){
		browser=Browser.current();
	}
	
	public void open() {
		browser = Browser.current();
		browser.click(TRASHBOX_LINK_LOCATOR);
	}

	public DeletedLetterPage findAndShowDeletedDraft(Letter letter) {
		browser = Browser.current();
		browser.waitForAppear(
				By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN,
						letter.getBody())));
		String urlDraft = browser.getElementAttribute(
				By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN,
						letter.getBody())), HREF_ATRIBUTE);
		browser.open(urlDraft);
		return new DeletedLetterPage();
	}
	
	public String getDeletedLetterAlertExist() {
		browser = Browser.current();
		browser.findElement(DELETED_LETTER_MESSAGE_LOCATOR);
		return browser.getText(DELETED_LETTER_MESSAGE_LOCATOR);
	}

	public Browser getBrowser() {
		return browser;
	}
}
