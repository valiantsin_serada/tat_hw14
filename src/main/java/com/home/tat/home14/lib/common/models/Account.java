package com.home.tat.home14.lib.common.models;

import lombok.Data;

@Data
public class Account {
	private String login;
	private String password;
	private String email;
}
