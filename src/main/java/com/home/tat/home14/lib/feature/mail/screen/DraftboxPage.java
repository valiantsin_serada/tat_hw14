package com.home.tat.home14.lib.feature.mail.screen;

import org.openqa.selenium.By;

import com.home.tat.home14.lib.feature.mail.models.Letter;
import com.home.tat.home14.lib.ui.Browser;

public class DraftboxPage implements IPageInterface {
	public static final By DRAFTBOX_LINK_LOCATOR = By
			.xpath("//a[@href='#draft']");
	public static final By STATUSLINE_MESSAGE_LOCATOR = By
			.xpath("//div[@class='b-statusline']");
	public static final By TRASHBOX_LINK_LOCATOR = By
			.xpath("//a[@href='#trash']");
	public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
	final String HREF_ATRIBUTE = "href";
	public Browser browser;
	
	public DraftboxPage(){
		browser=Browser.current();
	}
		 
	public void open()  {
		browser = Browser.current();
		browser.click(DRAFTBOX_LINK_LOCATOR);
	}

	public ComposePage findAndShowCreatedDraft(Letter letter) {
		browser = Browser.current();
		String urlDraft = browser.getElementAttribute(
				By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN,
						letter.getBody())), HREF_ATRIBUTE);
		browser.open(urlDraft);
		return new ComposePage();
	}

	public TrashboxPage switchToTrashboxAfterDeleting(Letter letter) {
		browser = Browser.current();
		browser.waitForDisappear(By.xpath(String.format(
				MAIL_LINK_LOCATOR_PATTERN, letter.getBody())));
		browser.refresh();
		browser.click(TRASHBOX_LINK_LOCATOR);
		return new TrashboxPage();
	}

	public Browser getBrowser() {
		return browser;
	}
}
