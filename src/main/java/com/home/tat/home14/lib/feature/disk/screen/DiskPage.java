package com.home.tat.home14.lib.feature.disk.screen;

import org.openqa.selenium.By;

import com.home.tat.home14.lib.common.constants.ILogingMessages;
import com.home.tat.home14.lib.ui.Browser;
import com.home.tat.home14.lib.util.Logger;

public class DiskPage {
	public static final String FILE_ELEMENT_LOCATOR_PATTERN = "//*[@class='nb-resource__name']//div[contains(., '%s')]";

	public static boolean isFilePresent(String fileName) {
		try {
			Browser.current().waitForAppear(
					By.xpath(String.format(FILE_ELEMENT_LOCATOR_PATTERN,
							fileName)));
		} catch (Exception e) {
			Logger.error(String.format(ILogingMessages.ERROR_OF_FILE_EXISTING,
					fileName), e);
			return false;
		}
		return true;
	}
}
