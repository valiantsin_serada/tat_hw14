package com.home.tat.home14.lib.ui;

public enum BrowserType {
	FIREFOX("firefox"), CHROME("chrome"), FIREFOX_LOCAL("firefox_local"), CHROME_LOCAL("chrome_local");

	String name;

	BrowserType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
