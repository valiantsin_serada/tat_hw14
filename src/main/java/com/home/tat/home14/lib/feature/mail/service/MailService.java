package com.home.tat.home14.lib.feature.mail.service;

import org.openqa.selenium.By;

import com.home.tat.home14.lib.common.constants.ILogingMessages;
import com.home.tat.home14.lib.feature.mail.models.Letter;
import com.home.tat.home14.lib.feature.mail.screen.ComposePage;
import com.home.tat.home14.lib.feature.mail.screen.DeletedLetterPage;
import com.home.tat.home14.lib.feature.mail.screen.DraftboxPage;
import com.home.tat.home14.lib.feature.mail.screen.IPageInterface;
import com.home.tat.home14.lib.feature.mail.screen.InboxPage;
import com.home.tat.home14.lib.feature.mail.screen.TrashboxPage;
import com.home.tat.home14.lib.util.Logger;

public class MailService {

	public void sendMail(Letter letter) {
		Logger.info(ILogingMessages.SEND_MAIL.concat(letter.toString()));
		ComposePage composePage = new ComposePage();
		composePage.open();
		composePage.sendMail(letter);
	}

	public void sendUnaddressedMail(Letter letter) {
		Logger.info(ILogingMessages.SEND_MAIL.concat(letter.toString()));
		ComposePage composePage = new ComposePage();
		composePage.open();
		composePage.sendUnaddressedMail(letter);
	}
	
	public boolean isLetterPresent(IPageInterface page, Letter letter) {
		try {
			page.getBrowser().waitForAppear(
					By.xpath(String.format(InboxPage.MAIL_LINK_LOCATOR_PATTERN,
							letter.getId())));
		} catch (Exception e) {
			Logger.error(String.format(ILogingMessages.ERROR_OF_LETTER_EXISTING,
					letter.getId()), e);
			return false;
		}
		return true;
	}
	
	public boolean checkDeliveryMail(Letter letter)	{
		InboxPage inboxPage = new InboxPage();
		inboxPage.open();
		return isLetterPresent(inboxPage, letter);
	}

	public String getErrorMessageInCaseOfEmptyAddressField() {
		ComposePage composePage = new ComposePage();
		return composePage.getErrorMessageInCaseOfEmptyAddressField();
	}

	public void createAndDeleteDraft(Letter letter) {
		Logger.info(ILogingMessages.CREATE_DRAFT.concat(letter.toString()));
		ComposePage composePage = new ComposePage();
		composePage.open();
		DraftboxPage draftboxPage = composePage.createDraft(letter);
		composePage = draftboxPage.findAndShowCreatedDraft(letter);
		draftboxPage = composePage.deleteDraft();
		TrashboxPage trashboxPage = draftboxPage
				.switchToTrashboxAfterDeleting(letter);
		DeletedLetterPage deletedLetterPage = trashboxPage
				.findAndShowDeletedDraft(letter);
		trashboxPage = deletedLetterPage.deleteLetter();
	}

	public String getMessageAboutDeleteLetter() {
		TrashboxPage trashboxPage = new TrashboxPage();
		return trashboxPage.getDeletedLetterAlertExist();
		}
}
