package com.home.tat.home14.lib.common.builders;

import static com.home.tat.home14.lib.common.constants.ICommonConstants.DEFAULT_MAIL_TO_SEND;
import static com.home.tat.home14.lib.common.constants.ICommonConstants.DEFAULT_MAIL_USER_LOGIN;
import static com.home.tat.home14.lib.common.constants.ICommonConstants.DEFAULT_MAIL_USER_PASSWORD;

import com.home.tat.home14.lib.common.models.Account;
import com.home.tat.home14.lib.util.Randomizer;

public class AccountBuilder {
	public static Account getDefaultAccount() {
		Account account = new Account();
		account.setLogin(DEFAULT_MAIL_USER_LOGIN);
		account.setPassword(DEFAULT_MAIL_USER_PASSWORD);
		account.setEmail(DEFAULT_MAIL_TO_SEND);
		return account;
	}

	public static Account getAccountWithWrongLogin() {
		Account account = getDefaultAccount();
		account.setLogin(account.getLogin().concat(Randomizer.alphabetic()));
		return account;
	}
}
