package com.home.tat.home14.lib.feature.disk.models;

import lombok.Data;

@Data
public class FileResource {
	String fileName;
	String content;	
}
