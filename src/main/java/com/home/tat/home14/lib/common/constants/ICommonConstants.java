package com.home.tat.home14.lib.common.constants;

public interface ICommonConstants {
	String BASE_YANDEX_URL = "http://www.ya.ru";
	String BASE_DISK_URL = "https://www.disk.yandex.ru";
	String PASSPORTPAGE_BASE_URL = "https://passport.yandex.ru";
	String DEFAULT_MAIL_USER_LOGIN = "tat-test-user@yandex.ru";
	String DEFAULT_MAIL_USER_PASSWORD = "tat-123qwe";
	String DEFAULT_MAIL_TO_SEND = "tat-test-user@yandex.ru";
	
	String DEFAULT_TESTFILE_NAME = "File.txt";
	String SCREENSHOTS_FILEPATH = "./tmp/screenshots/%d.png";

	String PASSPORT_PAGE_ERROR_MESSAGE = "Нет аккаунта с таким логином.";
	String EMPTY_ADDRESS_ERROR_MESSAGE = "Поле не заполнено. Необходимо ввести адрес.";
	String LETTER_DELETED_MESSAGE = "1 сообщение удалено.";
	String BROWSER_QUIT_ERROR_MESSAGE = "Problem with shutting down driver:";
}
