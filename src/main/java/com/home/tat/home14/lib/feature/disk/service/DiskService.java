package com.home.tat.home14.lib.feature.disk.service;

import java.io.IOException;
import java.util.List;

import com.home.tat.home14.lib.common.constants.ILogingMessages;
import com.home.tat.home14.lib.common.models.Account;
import com.home.tat.home14.lib.feature.disk.models.FileResource;
import com.home.tat.home14.lib.feature.disk.screen.LoginPage;
import com.home.tat.home14.lib.feature.disk.screen.MainDiskPage;
import com.home.tat.home14.lib.feature.disk.screen.TrashPage;
import com.home.tat.home14.lib.util.FileTools;
import com.home.tat.home14.lib.util.Logger;

public class DiskService {

	public void loginToDisk(Account account) {
		Logger.info(ILogingMessages.LOGIN_TO.concat(account.getEmail()));
		LoginPage loginPage = new LoginPage();
		loginPage.open();
		loginPage.loginAs(account);
	}
	
	public void openFolder() {
		Logger.info(ILogingMessages.OPEN_FOLDER.concat(MainDiskPage.STORE_FOLDER_NAME));
		MainDiskPage mainDiskPage = new MainDiskPage();
		mainDiskPage.openFolder();
	}
		
	public void uploadFile(String fileName) {
		Logger.info(ILogingMessages.UPLOAD_FILE.concat(fileName));
		MainDiskPage mainDiskPage = new MainDiskPage();
		mainDiskPage.uploadFile(fileName);
	}

	public void downloadFile(String fileName) {
		Logger.info(ILogingMessages.DOWNLOAD_FILE.concat(fileName));
		MainDiskPage mainDiskPage = new MainDiskPage();
		mainDiskPage.downloadFile(fileName);
	}

	public void dropeFilesToTrash(List<FileResource> fileResorces) {
		Logger.info(ILogingMessages.DROPE_FILES_TO_TRASH.concat(fileResorces
				.toString()));
		MainDiskPage mainDiskPage = new MainDiskPage();
		mainDiskPage.sendFilesToTrash(fileResorces);
		TrashPage trashPage = new TrashPage();
		trashPage.open();
	}

	public void deleteFilePermanently(FileResource fileResource) {
		Logger.info(ILogingMessages.DELETE_FILE_PERMANENTLY.concat(fileResource.getFileName()));
		TrashPage trashPage = new TrashPage();
		trashPage.deleteFilePermanently(fileResource);
	}

	public void deleteFiles(List<FileResource> fileResources) {
		Logger.info(ILogingMessages.DELETE_FILE_PERMANENTLY.concat(fileResources
				.toString()));
		TrashPage trashPage = new TrashPage();
		trashPage.deleteFiles(fileResources);
	}

	public void restoreFile(String fileName) {
		Logger.info(ILogingMessages.RESTORE_FILE.concat(fileName));
		TrashPage trashPage = new TrashPage();
		MainDiskPage mainDiskPage = new MainDiskPage();
		trashPage.restoreFile(fileName);
		mainDiskPage.open();
		mainDiskPage.openFolder();
	}

	public boolean checkContentsDownloadedAndSourceFiles(String sourceFilePath, String downloadedFilePath) throws IOException {
		return FileTools.compareContentsDownloadedAndUploadedFiles(sourceFilePath, downloadedFilePath);
	}

	public boolean isFileInFolder(String fileName) {
		return MainDiskPage.isFilePresent(fileName);
	}

	public boolean isFileInTrash(String fileName) {
		return TrashPage.isFilePresent(fileName);
	}

	public boolean isFilesInTrash(List<FileResource> fileResources) {
		return TrashPage.isFilesPresent(fileResources);
	}

	public boolean isFileDeletedPermanently(String fileName) {
		return TrashPage.isDeletingFileMessageExist(fileName);
	}
}
