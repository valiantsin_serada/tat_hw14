package com.home.tat.home14.lib.ui;

import java.util.HashMap;

import org.openqa.selenium.chrome.ChromeOptions;

import com.home.tat.home14.lib.util.FileTools;

public class ChromeBrowserOptionsBuilder {
	public static final String DEFAULT_CONTENT_SETTINGS_DECLARATION = "profile.default_content_settings.popups";
	public static final int DEFAULT_CONTENT_SETTINGS_PARAMETER = 0;
	public static final String DEFAULT_DOWNLOAD_DIRECTORY_DECLARATION = "download.default_directory";
	public static final String DOWNLOAD_FILEPATH = FileTools.DOWNLOAD_DIRECTORY.getAbsolutePath();
	public static final String EXPERIMENTAL_OPTIONS_SIGNATURE = "prefs";
	public static final String DOWNLOAD_FOLDERLIST_SIGNATURE = "--browser.download.folderList=2";
	public static final String OPTION_FOR_SKIP_DIALOGWINDOW_SIGNATURE = "--browser.helperApps.neverAsk.saveToDisk=text/plain";

	public static ChromeOptions buildChromeBrowserOptions() {
		ChromeOptions options = new ChromeOptions();
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put(DEFAULT_CONTENT_SETTINGS_DECLARATION,
				DEFAULT_CONTENT_SETTINGS_PARAMETER);
		chromePrefs.put(DEFAULT_DOWNLOAD_DIRECTORY_DECLARATION,
				DOWNLOAD_FILEPATH);
		options.setExperimentalOption(EXPERIMENTAL_OPTIONS_SIGNATURE,
				chromePrefs);
		options.addArguments(DOWNLOAD_FOLDERLIST_SIGNATURE);
		options.addArguments(OPTION_FOR_SKIP_DIALOGWINDOW_SIGNATURE);
		return options;
	}
}
