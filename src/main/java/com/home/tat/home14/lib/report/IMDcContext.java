package com.home.tat.home14.lib.report;

public interface IMDcContext {
	String REPORT_SUITE = "REPORT_SUITE";
	String REPORT_TEST = "REPORT_TEST";
	String REPORT_METHOD = "REPORT_METHOD";
}
