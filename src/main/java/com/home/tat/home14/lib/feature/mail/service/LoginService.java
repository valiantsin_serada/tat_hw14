package com.home.tat.home14.lib.feature.mail.service;

import com.home.tat.home14.lib.common.constants.ILogingMessages;
import com.home.tat.home14.lib.common.models.Account;
import com.home.tat.home14.lib.feature.mail.screen.InboxPage;
import com.home.tat.home14.lib.feature.mail.screen.MailLoginPage;
import com.home.tat.home14.lib.feature.mail.screen.PassportPage;
import com.home.tat.home14.lib.util.Logger;

public class LoginService {

	public void loginToMailbox(Account account) {
		Logger.info(ILogingMessages.LOGIN_TO.concat(account.getEmail()));
		MailLoginPage mailLoginPage = new MailLoginPage();
		mailLoginPage.open().login(account);
	}

	public boolean checkSuccessLogin(Account account) {
		InboxPage inboxPage = new InboxPage();
		return inboxPage.getCurrentAddress().contains(account.getEmail());
	}

	public boolean retrieveErrorOnFailedLogin(String expectedErrorMessage) {
		PassportPage passportPage = new PassportPage();
		return passportPage.getErrorMessage().equals(expectedErrorMessage);
	}
}
