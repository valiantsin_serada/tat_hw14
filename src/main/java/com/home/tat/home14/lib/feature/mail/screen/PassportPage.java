package com.home.tat.home14.lib.feature.mail.screen;

import org.openqa.selenium.By;

import com.home.tat.home14.lib.ui.Browser;

public class PassportPage {
	public static final By ERROR_MESSAGE_ELEMENT_LOCATOR = By
			.xpath("//div[@class='error-msg']");
	public Browser browser;
	
	public PassportPage(){
		browser=Browser.current();
	}

	public String getErrorMessage() {
		browser = Browser.current();
		browser.findElement(ERROR_MESSAGE_ELEMENT_LOCATOR);
		return browser.getText(ERROR_MESSAGE_ELEMENT_LOCATOR);
	}
}
