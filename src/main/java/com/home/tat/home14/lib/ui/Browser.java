package com.home.tat.home14.lib.ui;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.home.tat.home14.lib.common.constants.ICommonConstants;
import com.home.tat.home14.lib.common.constants.ILogingMessages;
import com.home.tat.home14.lib.feature.disk.screen.MainDiskPage;
import com.home.tat.home14.lib.util.FileTools;
import com.home.tat.home14.lib.util.Logger;

public class Browser implements WrapsDriver {
	public static final int LIMIT_10_SECONDS_FOR_FILE_DOWNLOADING = 10;
	public static final int LIMIT_10_SECONDS_FOR_FIND_ELEMENT = 10;
	public static final int LIMIT_10_SECONDS_FOR_WAIT_ELEMENT = 10;
	private static ThreadLocal<Browser> localThreadBrowser = new ThreadLocal<Browser>();
	private WebDriver driver;

	private Browser() {
	}

	private static Browser rise() {
		Browser browser = new Browser();
		browser.createDriver();
		localThreadBrowser.set(browser);
		return browser;
	}

	public static Browser current() {
		if (localThreadBrowser.get() == null) {
			rise();
		}
		return localThreadBrowser.get();
	}

	public void quit() {
		try {
			driver.quit();
		} catch (Exception ignore) {
		} finally {
			localThreadBrowser.remove();
		}
	}

	private void createDriver() {
		driver = WebDriverCreator.createDriver();
	}

	public WebDriver getWrappedDriver() {
		return driver;
	}

	public String getBrowserName() {
		return WebDriverCreator.browserCapabilities.getBrowserName();
	}

	public void open(String url) {
		Logger.debug(ILogingMessages.OPEN_PAGE.concat(url));
		getWrappedDriver().get(url);
	}

	public void waitForAppear(By locator) {
		Logger.debug(ILogingMessages.WAITING_APPEAR.concat(locator.toString()));
		try {
			new WebDriverWait(getWrappedDriver(),
					LIMIT_10_SECONDS_FOR_WAIT_ELEMENT).until(ExpectedConditions
					.presenceOfElementLocated(locator));
		} catch (Exception e) {
			Logger.error(String.format(ILogingMessages.ERROR_OF_FIND_ELEMENT,
					locator), e);
		}
	}

	public void waitForDisappear(By locator) {
		Logger.debug(ILogingMessages.WAITING_DISAPPEAR.concat(locator
				.toString()));
		try {
			new WebDriverWait(getWrappedDriver(),
					LIMIT_10_SECONDS_FOR_WAIT_ELEMENT).until(ExpectedConditions
					.invisibilityOfElementLocated(locator));
		} catch (Exception e) {
			Logger.error(String.format(ILogingMessages.ERROR_OF_FIND_ELEMENT,
					locator), e);
		}
	}

	public void click(By locator) {
		Logger.debug(ILogingMessages.CLICK_ON_ELEMENT.concat(locator.toString()));
		try {
			if(!locator.equals(MainDiskPage.UPLOAD_BUTTON_LOCATOR)){
			new WebDriverWait(getWrappedDriver(),
					LIMIT_10_SECONDS_FOR_WAIT_ELEMENT).until(ExpectedConditions
					.elementToBeClickable(locator));
			}
			getWrappedDriver().findElement(locator).click();
		} catch (Exception e) {
			Logger.error(String.format(ILogingMessages.ERROR_OF_FIND_ELEMENT,
					locator), e);
		}
	}

	public void doubleClick(By locator) {
		Logger.debug(ILogingMessages.DOUBLE_CLICK_ON_ELEMENT.concat(locator
				.toString()));
		waitForAppear(locator);
		Action action = new Actions(driver).doubleClick(findElement(locator))
				.build();
		action.perform();
	}

	public void submit(By locator) {
		Logger.debug(ILogingMessages.SUBMIT_ON_ELEMENT.concat(locator
				.toString()));
		getWrappedDriver().findElement(locator).submit();
	}

	public void waitForFileDownloading(final String fileName) {
		Logger.debug(ILogingMessages.WAITING_DOWNLOADING_FILE.concat(fileName));
		try {
			(new WebDriverWait(getWrappedDriver(),
					LIMIT_10_SECONDS_FOR_FILE_DOWNLOADING))
					.until(new ExpectedCondition<Boolean>() {
						public Boolean apply(WebDriver driver) {

							return new File((FileTools.DOWNLOAD_DIRECTORY
									.getAbsolutePath().concat(File.separator))
									.concat(fileName)).exists();
						}
					});
		} catch (TimeoutException e) {
			Logger.error(ILogingMessages.ERROR_OF_TIMEOUT_DOWNLOADING, e);
		} catch (Exception e) {
			Logger.error(ILogingMessages.ERROR_OF_FILE_DOWNLOADING, e);
		}
	}

	public String getText(By locator) {
		Logger.debug(ILogingMessages.GET_TEXT_OF_ELEMENT.concat(locator
				.toString()));
		return getWrappedDriver().findElement(locator).getText();
	}

	public void writeText(By locator, String text) {
		Logger.debug(String.format(ILogingMessages.WRITE_TEXT_TO_LOCATOR, text,
				locator));
		getWrappedDriver().findElement(locator).sendKeys(text);
	}

	public String getElementAttribute(By locator, String attribute) {
		Logger.debug(String.format(ILogingMessages.GET_ATTRIBUTE_OF_ELEMENT,
				attribute, locator.toString()));
		return getWrappedDriver().findElement(locator).getAttribute(attribute);
	}

	public void dropElementToTarget(Actions action, WebElement sourceElement,
			WebElement targetElement) {
		int XcoordinatOfSourceElement = sourceElement.getLocation().getX();
		int XcoordinatOfTargetElement = targetElement.getLocation().getX();
		int YcoordinatOfSourceElement = sourceElement.getLocation().getY();
		int YcoordinatOfTargetElement = targetElement.getLocation().getY();

		action.dragAndDropBy(sourceElement, XcoordinatOfTargetElement
				- XcoordinatOfSourceElement, YcoordinatOfTargetElement
				- YcoordinatOfSourceElement);
	}

	public WebElement findElement(By locator) {
		WebElement webElement = null;
		try {
			webElement = getWrappedDriver().findElement(locator);
		} catch (Exception e) {
			Logger.error(String.format(ILogingMessages.ERROR_OF_FIND_ELEMENT,
					locator), e);
		}
		return webElement;
	}

	public void refresh() {
		getWrappedDriver().navigate().refresh();
	}

	public void findAndClickFileElement(final String fileName) {
		(new WebDriverWait(getWrappedDriver(),
				LIMIT_10_SECONDS_FOR_FIND_ELEMENT))
				.until(new ExpectedCondition<Boolean>() {
					public Boolean apply(WebDriver driver) {
						try {
							click(By.xpath(String.format(
									MainDiskPage.FILE_ELEMENT_LOCATOR_PATTERN,
									fileName)));
						} catch (Exception e) {
							return false;
						}
						return true;
					}
				});
	}

	public void screenshot() throws IOException {
		if (getWrappedDriver() != null) {
			try {
				File screenshotFile = ((TakesScreenshot) getWrappedDriver())
						.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(
						screenshotFile,
						new File(String.format(
								ICommonConstants.SCREENSHOTS_FILEPATH,
								System.nanoTime())));
				Logger.debug(ILogingMessages.SCREENSHOT_TAKEN
						.concat(screenshotFile.getAbsolutePath()));
			} catch (Exception e) {
				Logger.error(ILogingMessages.ERROR_OF_SCREENSHOT_WRITING, e);
			}
		}
	}
}
