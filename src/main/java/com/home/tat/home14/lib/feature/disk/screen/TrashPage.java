package com.home.tat.home14.lib.feature.disk.screen;

import java.util.List;

import org.openqa.selenium.By;

import com.home.tat.home14.lib.common.constants.ILogingMessages;
import com.home.tat.home14.lib.feature.disk.models.FileResource;
import com.home.tat.home14.lib.ui.Browser;
import com.home.tat.home14.lib.util.FileTools;
import com.home.tat.home14.lib.util.Logger;

public class TrashPage extends DiskPage {
	public static final String BASE_URL = "https://disk.yandex.ru/client/trash";
	public static final String FILE_ELEMENT_LOCATOR_PATTERN = "//*[@class='nb-resource__name']//div[contains(., '%s')]";
	public static final By DELETE_BUTTON_LOCATOR = By
			.xpath("//button[@data-click-action='resource.delete']");
	public static final By RESTORE_BUTTON_LOCATOR = By
			.xpath("//button[@data-click-action='resource.restore']");
	public static final String MESSAGE_DELETING_FILE_PATTERN = "//div[text()='Файл «%s» был удален']";
	private Browser browser;

	public void open() {
		Browser.current().open(BASE_URL);
	}

	public TrashPage deleteFilePermanently(FileResource fileResource) {
		browser = Browser.current();
		deleteFiles(FileTools.wrapFileResourceByList(fileResource));
		return this;
	}

	public TrashPage restoreFile(String fileName) {
		browser = Browser.current();
		browser.refresh();
		browser.findAndClickFileElement(fileName);
		browser.click(RESTORE_BUTTON_LOCATOR);
		browser.waitForDisappear(By.xpath(String.format(
				FILE_ELEMENT_LOCATOR_PATTERN, fileName)));
		return this;
	}

	public void deleteFiles(List<FileResource> fileResources) {
		browser = Browser.current();
		for (FileResource fileResource : fileResources) {
			browser.refresh();
			browser.findAndClickFileElement(fileResource.getFileName());
			browser.click(DELETE_BUTTON_LOCATOR);
			browser.waitForAppear(By.xpath(String.format(
					MESSAGE_DELETING_FILE_PATTERN, fileResource.getFileName())));
		}
	}

	public static boolean isDeletingFileMessageExist(String fileName) {
		try {
			Browser.current().findElement(
					By.xpath(String.format(MESSAGE_DELETING_FILE_PATTERN,
							fileName)));
		} catch (Exception e) {
			Logger.error(ILogingMessages.ERROR_OF_FILE_DELETING, e);
			return false;
		}
		return true;
	}

	public static boolean isFilesPresent(List<FileResource> fileResources) {
		boolean fileExisting = false;
		for (FileResource fileResource : fileResources) {
			try {
				Browser.current().waitForAppear(
						By.xpath(String.format(FILE_ELEMENT_LOCATOR_PATTERN,
								fileResource.getFileName())));
			} catch (Exception e) {
				Logger.error(String.format(
						ILogingMessages.ERROR_OF_FILE_EXISTING,
						fileResource.getFileName()), e);
				fileExisting = false;
			}
			fileExisting = true;
		}
		return fileExisting;
	}

	public Browser getBrowser() {
		return browser;
	}
}
