package com.home.tat.home14.lib.runner.listeners;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import com.home.tat.home14.lib.common.constants.ILogingMessages;
import com.home.tat.home14.lib.ui.Browser;
import com.home.tat.home14.lib.util.Logger;

public class TestEventsListener extends TestListenerAdapter {
	@Override
	public void onStart(ITestContext testContext) {
		super.onStart(testContext);
		Logger.info(ILogingMessages.START_OF_TEST.concat(testContext.getName()));
	}

	@Override
	public void onTestFailure(ITestResult tr) {
		Logger.error(
				String.format(ILogingMessages.ERROR_TEST_FAILED,
						tr.getTestName()), tr.getThrowable());
		try {
			Browser.current().screenshot();
		} catch (Exception e) {
			Logger.error(ILogingMessages.ERROR_OF_SCREENSHOT_MAKING, e);
		}
	}

	@Override
	public void onFinish(ITestContext testContext) {
		super.onFinish(testContext);
		Browser.current().quit();
		Logger.info(ILogingMessages.FINISH_OF_TEST.concat(testContext.getName()));
	}	
}
