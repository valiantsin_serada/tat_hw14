package com.home.tat.home14.lib.ui;

import org.openqa.selenium.firefox.FirefoxProfile;

import com.home.tat.home14.lib.util.FileTools;

public class FirefoxBrowserProfileBuilder {
	public static final String DOWNLOAD_FOLDERLIST_SIGNATURE = "browser.download.folderList";
	public static final int DOWNLOAD_FOLDERLIST_PARAMETER = 2;
	public static final String DEFAULT_STARTING_BROWSER_SIGNATURE = "browser.download.manager.showWhenStarting";
	public static final boolean DEFAULT_STARTING_BROWSER_PARAMETER = false;
	public static final String DEFAULT_DOWNLOAD_DIRECTORY_DECLARATION = "browser.download.dir";
	public static final String DOWNLOAD_FILEPATH = FileTools.DOWNLOAD_DIRECTORY.getAbsolutePath();
	public static final String OPTION_FOR_SKIP_DIALOGWINDOW_SIGNATURE = "browser.helperApps.neverAsk.saveToDisk";
	public static final String TYPE_OF_FILES = "text/plain";

	public static FirefoxProfile buildFireFoxBrowserProfile() {
		FirefoxProfile firefoxProfile = new FirefoxProfile();
		firefoxProfile.setPreference(DOWNLOAD_FOLDERLIST_SIGNATURE,
				DOWNLOAD_FOLDERLIST_PARAMETER);
		firefoxProfile.setPreference(DEFAULT_STARTING_BROWSER_SIGNATURE,
				DEFAULT_STARTING_BROWSER_PARAMETER);
		firefoxProfile.setPreference(DEFAULT_DOWNLOAD_DIRECTORY_DECLARATION,
				DOWNLOAD_FILEPATH);
		firefoxProfile.setPreference(OPTION_FOR_SKIP_DIALOGWINDOW_SIGNATURE,
				TYPE_OF_FILES);
		return firefoxProfile;
	}
}
