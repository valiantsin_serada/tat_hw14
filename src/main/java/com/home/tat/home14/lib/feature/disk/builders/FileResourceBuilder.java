package com.home.tat.home14.lib.feature.disk.builders;

import java.util.ArrayList;
import java.util.List;

import com.home.tat.home14.lib.common.constants.ICommonConstants;
import com.home.tat.home14.lib.feature.disk.models.FileResource;
import com.home.tat.home14.lib.util.Randomizer;

public class FileResourceBuilder {

	public static FileResource getFileResource() {
		FileResource fileResource = new FileResource();
		fileResource.setContent(Randomizer.alphabetic());
		fileResource.setFileName(Randomizer.alphabetic().concat(
				ICommonConstants.DEFAULT_TESTFILE_NAME));
		return fileResource;
	}

	public static List<FileResource> getSeveralFileResources() {
		List<FileResource> listFileResources = new ArrayList<FileResource>();
		for (int i = 0; i < Randomizer.getRundomFilesNumber(); i++) {
			FileResource fileResource = new FileResource();
			fileResource.setContent(Randomizer.alphabetic());
			fileResource.setFileName(Randomizer.alphabetic().concat(
					ICommonConstants.DEFAULT_TESTFILE_NAME));
			listFileResources.add(fileResource);
		}
		return listFileResources;
	}
}
