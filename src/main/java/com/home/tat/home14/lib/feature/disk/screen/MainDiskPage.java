package com.home.tat.home14.lib.feature.disk.screen;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.home.tat.home14.lib.common.constants.ICommonConstants;
import com.home.tat.home14.lib.feature.disk.models.FileResource;
import com.home.tat.home14.lib.ui.Browser;
import com.home.tat.home14.lib.ui.BrowserType;
import com.home.tat.home14.lib.util.FileTools;

public class MainDiskPage extends DiskPage {
	public static final By UPLOAD_BUTTON_LOCATOR = By
			.xpath("//input[@class='button__attach']");
	public static final By UPLOAD_COMPLETE_MESSAGE = By
			.xpath("//div[text()='Загрузка завершена']");
	public static final By CLOSE_LINK_LOCATOR = By
			.xpath("//a[@data-click-action='dialog.close']");
	public static final By DOWNLOAD_BUTTON_LOCATOR = By
			.xpath("//div[contains(@class,'deselect ns-view-visible')]//ancestor::button[@data-click-action='resource.download']");
	public static final By TRASH_LINK_LOCATOR = By
			.xpath("//div[contains(@class,' slices ns-view-visible')]//div[@data-id='trash']//a[@href='/client/trash']");
	public static final String FILE_ELEMENT_LOCATOR_PATTERN = "//*[@class='nb-resource__name']//div[contains(., '%s')]";
	public static final String STORE_FOLDER_NAME = "TextFiles";
	private Browser browser;

	public void open() {
		Browser.current().open(ICommonConstants.BASE_DISK_URL);
	}

	public MainDiskPage uploadFile(final String fileName) {
		final String SCRIPT_FOR_CLICK = "arguments[0].click();";
		browser = Browser.current();
		browser.writeText(UPLOAD_BUTTON_LOCATOR,
				FileTools.getUploadedFilePath(fileName));
		if (browser.getBrowserName().equals(BrowserType.CHROME.getName())) {

			((JavascriptExecutor) browser.getWrappedDriver()).executeScript(
					SCRIPT_FOR_CLICK,
					browser.findElement(UPLOAD_BUTTON_LOCATOR));
		} else {
			browser.click(UPLOAD_BUTTON_LOCATOR);
		}
		browser.waitForAppear(UPLOAD_COMPLETE_MESSAGE);
		skipAlertMessage();
		browser.refresh();
		return this;
	}

	public MainDiskPage downloadFile(String fileName) {
		browser = Browser.current();
		browser.findAndClickFileElement(fileName);
		browser.click(DOWNLOAD_BUTTON_LOCATOR);
		browser.waitForFileDownloading(fileName);
		return this;
	}

	public MainDiskPage sendFilesToTrash(List<FileResource> fileResources) {
		browser = Browser.current();
		Actions action = new Actions(browser.getWrappedDriver());
		WebElement trashElement = browser.findElement(TRASH_LINK_LOCATOR);
		WebElement lastFileElement = null;
		action.keyDown(Keys.CONTROL);
		for (FileResource fileResource : fileResources) {
			browser.waitForAppear(By.xpath(String.format(
					FILE_ELEMENT_LOCATOR_PATTERN, fileResource.getFileName())));
			WebElement fileElement = browser.findElement(By.xpath(String
					.format(FILE_ELEMENT_LOCATOR_PATTERN,
							fileResource.getFileName())));
			action.click(fileElement);
			lastFileElement = fileElement;
		}
		browser.dropElementToTarget(action, lastFileElement, trashElement);
		action.keyUp(Keys.CONTROL).build().perform();
		return this;
	}

	public MainDiskPage openFolder() {
		browser = Browser.current();
		browser.waitForAppear(By.xpath(String.format(
				FILE_ELEMENT_LOCATOR_PATTERN, STORE_FOLDER_NAME)));
		browser.doubleClick(By.xpath(String.format(
				FILE_ELEMENT_LOCATOR_PATTERN, STORE_FOLDER_NAME)));
		return this;
	}

	public void skipAlertMessage() {
		browser = Browser.current();
		browser.click(CLOSE_LINK_LOCATOR);
	}

	public Browser getBrowser() {
		return browser;
	}
}
