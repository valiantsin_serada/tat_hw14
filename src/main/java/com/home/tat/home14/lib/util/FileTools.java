package com.home.tat.home14.lib.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.home.tat.home14.lib.feature.disk.models.FileResource;

public class FileTools {	
	 public static final File UPLOAD_DIRECTORY=FileUtils.getTempDirectory();
	 public static final File DOWNLOAD_DIRECTORY =new File(UPLOAD_DIRECTORY.getAbsolutePath().concat(Randomizer.alphabetic()));
	 
	 public static List<String> createTestFiles(FileResource... fileResources) throws IOException{
		 List<String> filesNames=new ArrayList<String>();
		 for(FileResource fileResource:fileResources){
			File testFile = new File(UPLOAD_DIRECTORY.getAbsolutePath().concat(File.separator).concat(fileResource.getFileName()));
			FileUtils.write(testFile, fileResource.getContent());
			filesNames.add(fileResource.getFileName()); 
		 }
		 return filesNames;
	 }
	 
	 public static void createTestFile(FileResource fileResource) throws IOException{
			File testFile = new File(UPLOAD_DIRECTORY.getAbsolutePath().concat(File.separator).concat(fileResource.getFileName()));
			FileUtils.write(testFile, fileResource.getContent());	
				 } 
	 
	 public static void createTestFiles(List<FileResource> fileResources) throws IOException{
			for(FileResource fileResource:fileResources){
		 	File testFile = new File(UPLOAD_DIRECTORY.getAbsolutePath().concat(File.separator).concat(fileResource.getFileName()));
			FileUtils.write(testFile, fileResource.getContent());	
			}
	 } 
	 
	 public static String getDownloadedFilePath(FileResource fileResource) throws IOException{
			return DOWNLOAD_DIRECTORY.getAbsolutePath().concat(File.separator).concat(fileResource.getFileName());
	 } 
	 
	 public static void createDownloadDirectory() throws IOException{
		 DOWNLOAD_DIRECTORY.mkdir();
	 } 
	 
	 public static String getUploadedFilePath(FileResource fileResource) throws IOException{
			return UPLOAD_DIRECTORY.getAbsolutePath().concat(File.separator).concat(fileResource.getFileName());
	 } 
	 
	 public static boolean compareContentsDownloadedAndUploadedFiles(String uploadedFilePath, String downloadedFilePath) throws IOException{
		 File sourceFile = new File(uploadedFilePath);
			File downloadedFile = new File(downloadedFilePath);
			return FileUtils.contentEquals(sourceFile, downloadedFile);
	 }

	public static String getUploadedFilePath(String fileName) {
		return UPLOAD_DIRECTORY.getAbsolutePath().concat(File.separator).concat(fileName);
	}
	
	public static List<FileResource> wrapFileResourceByList(FileResource fileResource) {
		List<FileResource> wrapedResource=new ArrayList<FileResource>();
		wrapedResource.add(fileResource);
		return  wrapedResource;
	}	
}
