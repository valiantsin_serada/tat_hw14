package com.home.tat.home14.lib.feature.mail.screen;

import org.openqa.selenium.By;

import com.home.tat.home14.lib.ui.Browser;

public class InboxPage implements IPageInterface {
	public static final By USER_NAME_ELEMENT_LOCATOR = By
			.xpath("//span[@class='header-user-name js-header-user-name']");
	public static final By INPOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
	public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(@href, '%s')]";
	public static final By COMPOSE_BUTTON_LOCATOR = By
			.xpath("//a[@href='#compose']");
	public static final By DELETE_BUTTON_LOCATOR = By
			.xpath("//a[@data-action='compose.delete']");
	public Browser browser;
	
	public InboxPage(){
		browser=Browser.current();
	}

	public void open() {
		browser = Browser.current();
		browser.click(INPOX_LINK_LOCATOR);
	}
	
	public String getCurrentAddress() {
		browser = Browser.current();
		browser.findElement(USER_NAME_ELEMENT_LOCATOR);
		return browser.getText(USER_NAME_ELEMENT_LOCATOR).trim();
	}

	public Browser getBrowser() {
		return browser;
	}
}
