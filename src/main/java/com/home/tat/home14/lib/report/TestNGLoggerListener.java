package com.home.tat.home14.lib.report;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.IResultListener2;

import com.home.tat.home14.lib.util.Logger;

import static com.home.tat.home14.lib.report.LoggerLevel.*;


public class TestNGLoggerListener implements IResultListener2 {

    public void beforeConfiguration(ITestResult itr) {
        Logger.log(CONF_STARTED, itr.getName());
    }

    public void onConfigurationSuccess(ITestResult itr) {
        Logger.log(CONF_SUCCESS, itr.getName());
    }

    public void onConfigurationFailure(ITestResult itr) {
        Logger.log(CONF_FAILED, itr.getName());
    }

    public void onConfigurationSkip(ITestResult itr) {
        Logger.log(CONF_SKIPPED, itr.getName());
    }

    public void onTestStart(ITestResult result) {
        Logger.log(METHOD_STARTED, result.getName());
    }

    public void onTestSuccess(ITestResult result) {
        Logger.log(METHOD_SUCCESS, result.getName());
    }

    public void onTestFailure(ITestResult result) {
        Logger.log(METHOD_FAILED, result.getTestClass().getRealClass().getSimpleName() + "." + result.getMethod().getMethodName(), result.getThrowable());
    }

    public void onTestSkipped(ITestResult result) {
        Logger.log(METHOD_SKIPPED, result.getName());
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
       
    }

    public void onStart(ITestContext context) {
        Logger.log(TEST_STARTED, context.getName());
    }

    public void onFinish(ITestContext context) {
        Logger.log(TEST_FINISHED, context.getName());
    }
}
