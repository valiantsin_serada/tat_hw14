package com.home.tat.home14.lib.feature.disk.screen;

import org.openqa.selenium.By;

import com.home.tat.home14.lib.common.constants.ICommonConstants;
import com.home.tat.home14.lib.common.models.Account;
import com.home.tat.home14.lib.ui.Browser;

public class LoginPage {
	public static final By LOGIN_INPUT_LOCATOR = By.name("login");
	public static final By PASSWORD_INPUT_LOCATOR = By.name("password");
    private Browser browser;
    
	public void open() {
		Browser.current().open(ICommonConstants.BASE_DISK_URL);
	}

	public MainDiskPage loginAs(Account account) {
		browser = Browser.current();
		browser.writeText(LOGIN_INPUT_LOCATOR, account.getLogin());
		browser.writeText(PASSWORD_INPUT_LOCATOR, account.getPassword());
		browser.submit(PASSWORD_INPUT_LOCATOR);
		return new MainDiskPage();
	}
}
