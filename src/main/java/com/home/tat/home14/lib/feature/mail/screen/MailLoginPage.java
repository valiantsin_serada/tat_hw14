package com.home.tat.home14.lib.feature.mail.screen;

import org.openqa.selenium.By;

import com.home.tat.home14.lib.common.constants.ICommonConstants;
import com.home.tat.home14.lib.common.models.Account;
import com.home.tat.home14.lib.ui.Browser;

public class MailLoginPage {

	public static final By ENTER_BUTTON_LOCATOR = By
			.xpath("//a[contains(@href, 'mail.yandex')]");
	public static final By LOGIN_INPUT_LOCATOR = By.name("login");
	public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
	private Browser browser;

	public MailLoginPage(){
		browser=Browser.current();
	}
	
	public MailLoginPage open() {
		browser = Browser.current();
		browser.open(ICommonConstants.BASE_YANDEX_URL);
		browser.click(ENTER_BUTTON_LOCATOR);
		MailLoginPage mailLoginPage = new MailLoginPage();
		browser.waitForAppear(MailLoginPage.LOGIN_INPUT_LOCATOR);
		return mailLoginPage;
	}

	public InboxPage login(Account account) {
		browser = Browser.current();
		browser.writeText(LOGIN_INPUT_LOCATOR, account.getLogin());
		browser.writeText(PASSWORD_INPUT_LOCATOR, account.getPassword());
		browser.submit(PASSWORD_INPUT_LOCATOR);
		return new InboxPage();
	}

	public PassportPage loginExpectedFailure(Account account) {
		browser = Browser.current();
		browser.writeText(LOGIN_INPUT_LOCATOR, account.getLogin());
		browser.writeText(PASSWORD_INPUT_LOCATOR, account.getPassword());
		browser.submit(PASSWORD_INPUT_LOCATOR);
		return new PassportPage();
	}
}
