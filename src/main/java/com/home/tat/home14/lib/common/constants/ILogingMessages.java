package com.home.tat.home14.lib.common.constants;

public interface ILogingMessages {
	String WRITE_TEXT_TO_LOCATOR = "Write text: \"%s\" to element: %s";
	String GET_ATTRIBUTE_OF_ELEMENT = "Get attribute: \"%s\" of element: %s";
	String OPEN_PAGE = "Open page : ";
	String WRITE_TEXT_TO = "Write text to : ";
	String TEXT_CONTENT = "text: ";
	String WAITING_APPEAR = "Waiting appear of element: ";
	String WAITING_CLICKABLE = "Waiting clickabe of element: ";
	String WAITING_DISAPPEAR = "Waiting disappear of element: ";
	String CLICK_ON_ELEMENT = "Click on element: ";
	String DOUBLE_CLICK_ON_ELEMENT = "Double click on element element: ";
	String SUBMIT_ON_ELEMENT = "Submit on element: ";
	String WAITING_DOWNLOADING_FILE = "Waiting downloading file: ";
	String GET_TEXT_OF_ELEMENT = "Get text of element: ";
	String ATTRIBUTE_NAME = "Attribute: ";

	String LOGIN_TO = "Login to: ";
	String OPEN_FOLDER = "Open folder: ";
	String UPLOAD_FILE = "Upload file: ";
	String DOWNLOAD_FILE = "Download file: ";
	String DELETE_FILE_PERMANENTLY = "Delete file permanently: ";
	String DROPE_FILES_TO_TRASH = "Drope files to trash: ";
	String RESTORE_FILE = "Restore file: ";
	String SEND_MAIL = "Send mail: ";
	String CREATE_DRAFT = "Create draft of letter: ";
	String SCREENSHOT_TAKEN = "Screenshot is taken by path: ";

	String START_TEST_METHOD = "Start of test method: ";
	String FINISH_TEST_METHOD = "Finish of test method: ";
	String START_CONFIGURATION_METHOD = "Start of configuration method: ";
	String FINISH_CONFIGURATION_METHOD = "Finish of configuration method: ";
	
	String START_OF_TESTS = "Tests will be started";
	String START_OF_TEST = "Start of test: ";
	String FINISH_OF_TEST = "Finish of test: ";
	String PARSING_CLI_PARAMETERS="Parse cli params...";

	String ERROR_OF_FILE_DOWNLOADING = "Error of file downloading: ";
	String ERROR_TEST_FAILED = "The test \"%s\"is failed: ";
	String ERROR_OF_SCREENSHOT_MAKING = "Error of screenshot making: ";
	String ERROR_OF_BROWSER_CLOSING = "Error of browser closing";
	String ERROR_OF_FILE_EXISTING = "The file\"%s\" is not exist";
	String ERROR_OF_FILE_DELETING = "The file\"%s\" is not deleted";
	String ERROR_OF_FIND_ELEMENT = "Error of finding element: %s";
	String ERROR_OF_LETTER_EXISTING = "The letter with ID:\"%s\" is not exist";
	String ERROR_OF_UNADDRESSED_MAIL_SENDING = "Error of appearing alert message of unaddressed letter";
	String ERROR_OF_PARSING_CLI_PARAMETERS = "Failed to parse cli params: ";
	String ERROR_OF_REMOTE_CONNECTION = "Error of  connection to remote port: ";
	String ERROR_OF_TIMEOUT_DOWNLOADING = "Error of timeout for file downloading";
	String ERROR_OF_SCREENSHOT_WRITING = "Error of screenshot writing: ";	
}
