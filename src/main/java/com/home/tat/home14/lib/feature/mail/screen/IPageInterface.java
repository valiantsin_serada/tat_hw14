package com.home.tat.home14.lib.feature.mail.screen;

import com.home.tat.home14.lib.ui.Browser;

public interface IPageInterface {
	public void open();
	public Browser getBrowser();
}
