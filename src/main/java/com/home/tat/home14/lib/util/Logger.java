package com.home.tat.home14.lib.util;

import org.apache.log4j.Priority;

public class Logger {
	private static org.apache.log4j.Logger logger = org.apache.log4j.Logger
			.getLogger("com.home.tat.home14");

	public static void info(String message) {
		logger.info(message);
	}

	public static void error(String message, Throwable e) {
		logger.error(message, e);
	}

	public static void debug(String message) {
		logger.debug(message);
	}

	public static void log(Priority priority, String message, Throwable t) {
		logger.log(priority, message, t);
	}

	public static void log(Priority priority, String message) {
		logger.log(priority, message);
	}
}
