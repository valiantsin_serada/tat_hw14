package com.home.tat.home14.lib.ui;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.home.tat.home14.lib.common.constants.ILogingMessages;
import com.home.tat.home14.lib.config.GlobalConfig;
import com.home.tat.home14.lib.util.Logger;

public class WebDriverCreator {
	public static final String LOCAL_CHROME_DRIVER_SIGNATURE = "webdriver.chrome.driver";
	public static final String PATH_TO_LOCAL_CHROME_DRIVER = "./src/main/resources/chromedriver.exe";
	public static final int LIMIT_FOR_PAGE_LOAD_20 = 20;
	public static final int DRIVE_IMPL_WAIT_TIMEOUT_10 = 10;
	public static DesiredCapabilities browserCapabilities;
	private static WebDriver driver;
	
	private static String remoteServerUrl = GlobalConfig.getInstance()
			.getRemoteServerUrl();
	
	public static WebDriver createDriver() {
		switch (BrowserType.valueOf(GlobalConfig.getInstance().getBrowserType()
				.getName().toUpperCase())) {
		case FIREFOX:
			setFirefoxBrowser();
			break;
		case CHROME:
			setChromeBrowser();
			break;
		case FIREFOX_LOCAL:
			setFirefoxLocalBrowser();
			break;
		case CHROME_LOCAL:
			setChromeLocalBrowser();
			break;		
		}
		
		Timeouts driverTimeouts = driver.manage().timeouts();
		driverTimeouts.pageLoadTimeout(LIMIT_FOR_PAGE_LOAD_20,
				TimeUnit.SECONDS);
		driverTimeouts.implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_10,
				TimeUnit.SECONDS);
		driver.manage().window().maximize();
		return driver;
	}
	
	private static void setFirefoxBrowser() {
		try{
		FirefoxProfile firefoxProfile = FirefoxBrowserProfileBuilder
				.buildFireFoxBrowserProfile();
		browserCapabilities = DesiredCapabilities.firefox();
		browserCapabilities
				.setCapability(FirefoxDriver.PROFILE, firefoxProfile);
		driver = new RemoteWebDriver(new URL(remoteServerUrl),
				browserCapabilities);
		}catch(MalformedURLException e){
			Logger.error(ILogingMessages.ERROR_OF_REMOTE_CONNECTION, e);
		}		
	}

	private static void setFirefoxLocalBrowser() {
		FirefoxProfile firefoxProfile = FirefoxBrowserProfileBuilder
				.buildFireFoxBrowserProfile();
		browserCapabilities = DesiredCapabilities.firefox();
		browserCapabilities
				.setCapability(FirefoxDriver.PROFILE, firefoxProfile);
		driver = new FirefoxDriver(browserCapabilities);
	}

	private static void setChromeBrowser() {
		try{
		ChromeOptions options = ChromeBrowserOptionsBuilder.buildChromeBrowserOptions();
		browserCapabilities = DesiredCapabilities.chrome();
		browserCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new RemoteWebDriver(new URL(remoteServerUrl),
				browserCapabilities);
		}catch(MalformedURLException e){
			Logger.error(ILogingMessages.ERROR_OF_REMOTE_CONNECTION, e);
		}
	}

	private static void setChromeLocalBrowser() {
		System.setProperty(LOCAL_CHROME_DRIVER_SIGNATURE,
				PATH_TO_LOCAL_CHROME_DRIVER);
		ChromeOptions options = ChromeBrowserOptionsBuilder.buildChromeBrowserOptions();
		browserCapabilities = DesiredCapabilities.chrome();
		browserCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(browserCapabilities);
	}
}
