package com.home.tat.home14.lib.feature.mail.screen;

import org.openqa.selenium.By;

import com.home.tat.home14.lib.ui.Browser;

public class DeletedLetterPage {
	public static final By PERMANENTDELETE_BUTTON_LOCATOR = By
			.xpath("//a[@data-action='delete']");
	public Browser browser;
		
	public DeletedLetterPage(){
		browser=Browser.current();
	}

	public TrashboxPage deleteLetter() {
		Browser.current().click(PERMANENTDELETE_BUTTON_LOCATOR);
		return new TrashboxPage();
	}
}
